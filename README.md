# Git Tools

## Install

    ./install

Be sure having `~/.bin` in your `$PATH` variable.

Zsh completion is added to `~/.completions` folder.

## Usage

- branch-missing: show branches not merged with current branch
- branch-missing-diff: show diff from branches not merged with current branch
- branch-add: Create a new branch from current branch
- branch-wipe: Remove local branch which have no matching remote branch
- tag-delete: Delete a tag
- tag-release: Create a release tag
- tag-version: Create a version tag
- commit-missing: Missing commit : show commit from branch not merged in current branch
- commit-undo: Undo last commit
- commit-amend: Modify last commit (not pushed)
- commit-show: show commit
- commit-show-diff: show commit with full diff
- st: show status of current git folder
- st-recurse: show status of all git folder in current folder
- tree: show commit tree
- tree-stats: shot commit tree with change stats
- local-commit: show local commit (not pushed)
- local-commit-stats: show local commit (not pushed) with change stats
- stash-save: save stash with message
- stash-pop: pop stash
- pull-safe: pull (stash all before)
- pull-all: pull all branches  (stash all before)
- push-all: push all branches and tags
- merge-no-ff: merge branches without fast-forward
- rebase-safe: rebase interactive and no-ff
- co: easy checkout branch
- feature-add: add a new feature branch from relase branch
- feature-close: close current feature branch by merge it with master branch

## License

Copyright © 2016  Guillaume Zugmeyer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
